package com.mhst.architecture_with_coroutines.app

import android.app.Application
import android.content.Context

/**
 * Created by Moe Htet on 29,April,2020
 */

class MyApp : Application(){

    companion object{
        lateinit var context : Context
    }

    override fun onCreate() {
        super.onCreate()
        context = applicationContext;
    }
}