package com.mhst.architecture_with_coroutines.data.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mhst.architecture_with_coroutines.data.vo.MovieVO

/**
 * Created by Moe Htet on 30,April,2020
 */


class TypeConverter{

    var gson = Gson()

    @TypeConverter
    fun stringToList(data : String) : List<MovieVO>{
        val listType = object : TypeToken<List<MovieVO>>() {}.type
        return if(data == null) listOf()
        else gson.fromJson(data,listType)
    }

    @TypeConverter
    fun listToString(list : List<MovieVO>) : String{
        return gson.toJson(list)
    }


}