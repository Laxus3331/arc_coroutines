package com.mhst.architecture_with_coroutines.data.db

import androidx.room.Room
import com.mhst.architecture_with_coroutines.app.MyApp.Companion.context

/**
 * Created by Moe Htet on 30,April,2020
 */
object DatabaseProvider {

    var appDatabase : AppDatabase = Room.databaseBuilder(context.applicationContext,AppDatabase::class.java,"movies")
        .fallbackToDestructiveMigration()
        .build()

}