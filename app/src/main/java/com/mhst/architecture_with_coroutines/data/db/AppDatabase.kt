package com.mhst.architecture_with_coroutines.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.mhst.architecture_with_coroutines.data.vo.MovieVO

/**
 * Created by Moe Htet on 30,April,2020
 */

@Database(entities = [MovieVO::class],exportSchema = false,version = 2)
@TypeConverters(TypeConverter::class)
abstract class AppDatabase  : RoomDatabase(){

    abstract  fun movieDao() : MovieDao

}