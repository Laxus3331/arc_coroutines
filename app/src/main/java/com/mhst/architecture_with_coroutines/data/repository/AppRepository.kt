package com.mhst.architecture_with_coroutines.data.repository

import android.util.Log
import com.mhst.architecture_with_coroutines.API_KEY
import com.mhst.architecture_with_coroutines.data.db.DatabaseProvider
import com.mhst.architecture_with_coroutines.data.network.ApiProvider
import com.mhst.architecture_with_coroutines.data.vo.MovieVO

/**
 * Created by Moe Htet on 30,April,2020
 */
class AppRepository {

  private  val network = ApiProvider.api;

   private val database = DatabaseProvider.appDatabase;

    suspend fun getMovies() : List<MovieVO>{

        var cacheMovies = database.movieDao().getAllMovies();

        if(cacheMovies == null || cacheMovies.isEmpty()){
            Log.d("API","is chosen")
          var movies = network.getUpcomingMovies(API_KEY).results
           database.movieDao().refreshMovies(movies)
            return movies
        }
        else{
            Log.d("Database","is chosen")
            return cacheMovies;
        }

    }



}