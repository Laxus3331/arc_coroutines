package com.mhst.architecture_with_coroutines.data.vo

import com.google.gson.annotations.SerializedName

/**
 * Created by Moe Htet on 29,April,2020
 */
data class MovieResponse(
    @SerializedName("page") val page:Int = 0,
    @SerializedName("results") val results:List<MovieVO> = listOf(),
    @SerializedName("total_results") val totalResults:Int = 0,
    @SerializedName("total_pages") val totalPages:Int = 0
)