package com.mhst.architecture_with_coroutines.data.vo

/**
 * Created by Moe Htet on 29,April,2020
 */

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "my_movies")
data class MovieVO (
    @PrimaryKey
    @SerializedName("id") val movieId : Long = 0,
    @SerializedName("adult") val adult : Boolean = false,
    @ColumnInfo(defaultValue = "")
    @SerializedName("poster_path") val imgUrl : String = "",
    @SerializedName("runtime") val runtime : Long = 0,
    @SerializedName("title") val title : String = "",
    @SerializedName("overview") val overview : String = "",
    @SerializedName("release_date") val release_date : String = ""
)