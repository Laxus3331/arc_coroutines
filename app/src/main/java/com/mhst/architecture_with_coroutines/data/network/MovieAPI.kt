package com.mhst.architecture_with_coroutines.data.network

import com.mhst.architecture_with_coroutines.data.vo.MovieResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Moe Htet on 29,April,2020
 */
interface MovieAPI {

    @GET("movie/upcoming")
   suspend fun getUpcomingMovies(@Query("api_key") apiKey : String) : MovieResponse

}