package com.mhst.architecture_with_coroutines.data.db

import androidx.room.*
import com.mhst.architecture_with_coroutines.data.vo.MovieVO

/**
 * Created by Moe Htet on 29,April,2020
 */
@Dao
interface MovieDao {

    @Query("select * from my_movies")
    suspend fun getAllMovies() : List<MovieVO>

    @Query("delete from my_movies")
    suspend fun clearAllMovies()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAllMovies(movies : List<MovieVO>)

    @Transaction
    suspend fun refreshMovies(movies : List<MovieVO>){
        clearAllMovies()
        addAllMovies(movies)
    }



}