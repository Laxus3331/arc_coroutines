package com.mhst.architecture_with_coroutines

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import coil.api.load
import com.bumptech.glide.Glide
import com.list.rados.fast_list.bind
import com.mhst.architecture_with_coroutines.data.vo.MovieVO
import com.mhst.architecture_with_coroutines.viewmodel.AppViewModel
import com.mhst.architecture_with_coroutines.viewstate.MainViewState
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.movie_card.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {

    lateinit var appViewModel: AppViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        appViewModel = ViewModelProvider(this)[AppViewModel::class.java]

        appViewModel.viewState.observe(this, Observer {
            renderViewState(it)
        })

        swipeRefreshLayout.setOnRefreshListener {
            lifecycleScope.launch {
                appViewModel.loadMovies()
            }
        }

    }

    private fun renderViewState(viewState: MainViewState){
        when(viewState){
            is MainViewState.loading -> swipeRefreshLayout.isRefreshing = true
            is MainViewState.onSucess -> {
                swipeRefreshLayout.isRefreshing = false
                rvMovies.bind(viewState.movies,R.layout.movie_card){ it : MovieVO, position: Int ->
                        movie_title.text = it.title
                        release_year.text = it.release_date
                        movie_poster.load(BASE_IMGAE_URL+it.imgUrl)
                }.layoutManager(GridLayoutManager(this,3))
                Log.d("MoviesLength",viewState.movies.size.toString())
            }
            is MainViewState.onFailure -> {
                Log.d("errorFetch",viewState.error)
                swipeRefreshLayout.isRefreshing = false;
                Toast.makeText(this,viewState.error,Toast.LENGTH_SHORT).show()
            }
        }
    }
}
