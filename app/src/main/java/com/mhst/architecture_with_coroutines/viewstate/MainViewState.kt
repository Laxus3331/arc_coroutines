package com.mhst.architecture_with_coroutines.viewstate

import com.mhst.architecture_with_coroutines.data.vo.MovieVO

/**
 * Created by Moe Htet on 30,April,2020
 */

sealed class MainViewState {

    object loading : MainViewState()

    data class onSucess(val movies : List<MovieVO>) : MainViewState()

    data class onFailure(val error : String) : MainViewState()

}