package com.mhst.architecture_with_coroutines.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.mhst.architecture_with_coroutines.data.repository.AppRepository
import com.mhst.architecture_with_coroutines.viewstate.MainViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Dispatcher
import java.lang.Exception

/**
 * Created by Moe Htet on 30,April,2020
 */

class AppViewModel : ViewModel(){

    private val repository = AppRepository()

    val viewState = MutableLiveData<MainViewState>()

    init {
        viewModelScope.launch {
            loadMovies()
        }
    }

    suspend fun loadMovies(){
        viewState.postValue(MainViewState.loading)
        withContext(Dispatchers.IO){
            try {
                viewState.postValue(MainViewState.onSucess(repository.getMovies()))
            }catch (e : Exception){
                viewState.postValue(MainViewState.onFailure(e.localizedMessage))
            }
        }
    }


}

